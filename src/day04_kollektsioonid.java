import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class day04_kollektsioonid {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[] arr1 = { 5, 7, 9, 4, 6 };
		System.out.println(arr1[2]); // prindi v�lja arr1 kohal 2
		arr1[2] = 98;
		System.out.println(arr1[2]);
		System.out.println(arr1.length);

		List<Integer> list1 = new ArrayList<>(Arrays.asList(5, 7, 9, 4, 6)); // List hoiab endas listi t�is arve,mitte
																				// primitiivist. // lisame listi
																				// elementid ( arr1= {5, 7, 2, 4, 6 } )
		list1.add(0, 65); // ADD-LISA 0 kohale number 65
		System.out.println(list1);
		list1.add(3, 100); // prindi kolmanda elementina number "100"
		System.out.println(list1);
		list1.remove(1); // eemalda teise numbri massiivist ehk "5" {65 on "0", 5 (on eismene number)
							// {65, 5, 7, 9, 4, 6 };

		System.out.println(list1.size()); // size �tleb mitu tk on massiivis {65, 5, 7, 9, 4, 6 };
		list1.forEach(x -> {
			System.out.println("Prindin v�lja elemendi...");
			System.out.println("elemendi v��rtus ");
			System.out.println("viimane element");
		});

		// HashSet
		Set<String> set1 = new HashSet<String>(Arrays.asList("viis", " kuus ", " seitse "));
		System.out.println(set1);
		set1.add(" seitse ");
		set1.add(" kaheksa ");
		String[] elementsFromSet = set1.toArray(new String[0]);
		set1.removeIf(x -> x.equals(" viis ") || x.equals(" kuus ")); // removeIf ehk
		// eemalda X v��rtused ,mis on "viis" || (v�i ja) "kuus" v��rtusegfa
		set1.remove("viis");
		System.out.println(set1);

		// Treeset
		//
		// Set<String> set2 = new TreeSet<String>(Arrays.asList("viis", "kuus",
		// "seitse"));
		// System.out.println(set2);

	}

}
