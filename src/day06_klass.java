import java.util.Scanner;

import day07.Gender;
import koer.Dog;
import koer.Person;

public class day06_klass {

	public static void main(String[] args) {
		// OoBasics ooBasics=new OoBasics (); // teen objekti klassist endast

		Dog dog = new Dog();
		// dog.name = "Muki";
		// dog.tailLenght = 40;
		// System.out.println(dog.legCount);
		// System.out.println(dog.name);
		dog.bark();

		Scanner scanner = new Scanner(System.in);

		Dog dog2 = new Dog();

		Person person1 = new Person("48807172748");
		System.out.println(person1.getBirthYear());
		System.out.println(person1.getBirthMonth());
		System.out.println(person1.getBirthDayOfMonth());
		if (person1.getGender() == Gender.MALE) {
			System.out.println("The person is Male");
		} else if (person1.getGender() == Gender.FEMALE) {
			System.out.println("The person is Female");
		} else {
			System.out.println("the person is unknown");

		}
	}

}
