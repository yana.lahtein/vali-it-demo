package day03;

public class If {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int number = Integer.parseInt(args[0]);

		boolean oddityCheck = number % 2 == 0;

		if (oddityCheck) {
			System.out.println("Etteantud arv on paaritu");
		} else {
			System.out.println("Etteantud arv on paaris");

		}

		String vastus = (number % 2 == 0) ? "Etteantud arv " + number + " paaris"
				: "Etteantud arv " + number + " paaritu";

		System.out.println(vastus);

		System.out.println(
				(number % 2 == 0) ? "Etteantud arv " + number + " paaris" : " Etteantud arv " + number + "paaritu");

		// Switch n�ide
		int doorStatus = 1;
		switch (doorStatus) {
		case 0:
			System.out.println("Door is closed");
			break;
		case 1:
		case 2:
			System.out.println("Door is very open");
			System.out.println("Door is little bit open");
			break;
		case 3:
			System.out.println("Door is very open");
			break;
		case 4:
			System.out.println("Door is gone!");
			break;
		default:
			System.out.println("Could not complete thew command");

		}
		
		String color=args[0].toUpperCase();
			if ("GREEN".equals(color)) {
			System.out.println("Driver can drive a car. ") ; 
		} else if ("YELLOW".equals(color) {
			System.out.println("Driver has to be ready to stop the car or to start driving.");
		}
	
		switch (color) {
		case "GREEN":
			System.out.println("Driver can drive a car");
			break;
		case "YELLOW":
			System.out.println("Driver has to be ready to stop the car or to start driving." );
			break;
		case "RED":
			System.out.println("Driver has to stop car and wait for green light");
		default:
			System.out.println("Driver has to stop car and wait for green light");
			
		}
		
	}
}
