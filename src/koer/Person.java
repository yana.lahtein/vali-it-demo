package koer;

import java.math.BigInteger;

import day07.Gender;

public class Person {

	private String personCode;

	public Person(String personCode) { // konstruktor meetod
		this.personCode = personCode; // salvesta see ajutiselt enda klassi

	}

	public int getBirthYear() {
		if (this.personCode != null && this.personCode.length() == 11) {
			int centuryDeterminant = Integer.parseInt(this.personCode.substring(0, 1));
			int birthDeterminant = Integer.parseInt(this.personCode.substring(1, 3));

			if (centuryDeterminant == 1 || centuryDeterminant == 2) {
				return 1800 + birthDeterminant;
			} else {
				return 0;
			}

		}
		return 0;
	}

	public String getBirthMonth() {
		if (personCode != null && personCode.length() == 11) {
			String BirthMonthNumber = personCode.substring(4, 6);
			switch (BirthMonthNumber) {
			case "01":
				return "Jaanuar";
			case "02":
				return "Veebruar";
			case "03":
				return "M�rts";
			case "04":
				return "Aprill";
			case "05":
				return "Mai";
			case "06":
				return "Juuni";
			case "07":
				return "Juuli";
			case "08":
				return "August";
			case "09":
				return "September";
			case "10":
				return "Oktoober";
			case "11":
				return "November ";
			case "12":
				return "Detsember";
			}
		}
		return "teadmata";
	}

	public String getBirthDayOfMonth() {
		if (personCode != null && personCode.length() == 11) {
			return personCode.substring(5, 7);
		}
		return null;
	}

	public Gender getGender() {
		if (personCode != null) {
			int genderDeterminant = Integer.parseInt(personCode.substring(0, 1));
			if (genderDeterminant % 2 == 0) {
				return Gender.FEMALE;
			} else {
				return Gender.MALE;

			}
		}
		return Gender.UNKNOWN;
	}

	public static boolean IsPersonalCodeCorrect(String personalCode) {
		
		boolean getBirthDayOfMonth;
		boolean result= getBirthDayOfMonth (new BigInteger (personalCode));
			if (result) {
				int month=Integer.parseInt(personalCode.substring(3, 5));
				result=month >0 && month < 13;
			}
			int month=Integer.parseInt(personalCode.substring(3,5));
			int day=Integer.parseInt(personalCode.substring(5, 7));
			if (result) {

			}
			if (month==2) {
				result =day> 0 && day <=29;
			} else {
			switch (month) {
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
			case 9:
			case 10:
			case 11:
			case 12:
				result= day > 0 && day < 32;
				break;
			}
			default:
				result=day>0 && day<31;
		}
			
			return result;
	}

}
