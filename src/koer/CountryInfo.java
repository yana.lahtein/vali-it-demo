package koer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CountryInfo {

	public String name;
	public String capital;
	public String primeMinister;
	public List<String> languages = new ArrayList<>();

	public CountryInfo(String name, String capital, String primeMinister, Collection<String> languages) { // konstruktor
																											// meetod
		this.name = name;
		this.capital = capital;
		this.primeMinister = primeMinister;
		this.languages.addAll(languages);
	}
	
	Dog.printDogCount();
	

	@Override
	public String toString() {
		String textOutput = this.name;
		textOutput += "\n";
		for (String language : this.languages) {
			textOutput += " " + language + "\n";
		}
		return textOutput;

	}

	// return super.toString() // super on pöördumine parent klassi

	// }public String toString() {
	// return getClass().getName() + "@" + Integer.toHexString(hashCode());

}
