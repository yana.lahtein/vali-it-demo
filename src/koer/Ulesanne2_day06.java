package koer;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Ulesanne2_day06 {

	public static void main(String[] args) {

		CountryInfo estonia = new CountryInfo("Estonia", "Tallinn", "J�ri Ratas", Arrays.asList("Est", "RUS"));
		estonia.name = "Estonia";
		estonia.capital = "Tallinn";
		estonia.primeMinister = "J�ri Ratas";
		estonia.languages.addAll(Arrays.asList("Est", "Rus", "Latv"));

		

		CountryInfo latvia = new CountryInfo("Latvia","Riga","Maris",Arrays.asList("Est", "RUS"));
		latvia.name = "Latvia";
		latvia.capital = "Riga";
		latvia.primeMinister = "Maris";
		latvia.languages.addAll(Arrays.asList("Latv", "Rus", "Fin"));

		List<CountryInfo> countries = new ArrayList<>();
		countries.add(estonia);
		countries.add(latvia);

		for (CountryInfo country : countries) {
			System.out.println(country);
	
		}
		
		
	}
	

}
