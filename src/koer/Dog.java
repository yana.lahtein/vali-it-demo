package koer;

public class Dog {

	protected int legCount = 4; // seisundit,paljundatakse iga objektile kaasa
	private int tailLenght = 0; // seisund

	public int getLegCount() {
		return legCount;
	}

	public void setLegCount(int legCount) {
		this.legCount = legCount;
	}

	public int getTailLenght() {
		return tailLenght;
	}

	public void setTailLenght(int tailLenght) {
		this.tailLenght = tailLenght;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static int getCount() {
		return count;
	}

	public static void setCount(int count) {
		Dog.count = count;
	}

	public int getAge() {
		return age;
	}

	private String name = null; // seisund
	private int age = 0;

	// Getter ja Setter on hea tava markeerida klassi muutujad.
	public int getlegCount() { // get muster-meetod. Saab kirjutada mingi logi. Annab suurema kontrolli.
		System.out.println("The dog " + this.name + "leg was requested");
		return this.legCount;
	}

	public void setAge(int dogAge) { // set muster-meetod annab suurema kontrolli

		if (dogAge > 0 && dogAge < 25) {
			this.age = dogAge;
		} else {
			System.out.println(" Invalid age setting attemp to " + this.name);
		}
	}

	public static int count = 0; //


	public Dog() {
		Dog.count++;

	}

	public void bark() { // koeral on omadus-oskab aukuda
		System.out.println("Barking...");
	}

	public void run() {
		if (age > 5) {
			System.out.println("Run slowly");
		} else {
			System.out.println("Run fast");
		}
	}

	public static void printDogCount() { // static - staatiline
		System.out.println("bbbb" + Dog.count + "mmm");
	}

	public void printFellowCount() { // ei ole static mittestaatiline
		System.out.println("Text" + Dog.count); // count muutuja

	}
}
