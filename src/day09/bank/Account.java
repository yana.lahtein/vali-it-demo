package day09.bank;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class Account {

	private String firstname;
	private String lastname;
	private String accountNumber;
	private int balance;

	// defineerin staatilise muutuja, t��biks paneme Map,mis koosneb < key, value)

	private static Map<String, Account> clients = new HashMap<>();

	// generate Constructor

	public Account(String firstname, String lastname, String accountNumber, int balance) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.accountNumber = accountNumber;
		this.balance = balance;
	}

	public static Map<String, Account> getClients() {
		return clients;
	}

	public static void setClients(Map<String, Account> clients) {
		Account.clients = clients;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	@Override
	public String toString() {

		return String.format("[%s, %s, %s, %s]", this.getFirstname(), this.getLastname(), this.getAccountNumber(),
				this.getBalance());
	}

	public static void importClients(String filePath) throws IOException {
		Path path = Paths.get(filePath);
		List<String> filelines = Files.readAllLines(path);
		for (String fileline : filelines) {
			String[] lineParts = fileline.split(", ");

			// loome Mapi
			clients.put(lineParts[2],
					new Account(lineParts[0], lineParts[1], lineParts[2], Integer.parseInt(lineParts[3]))); // lineparts
																											// on konto
																											// nr faili
																											// sees
		}

	}

	public static void displayClientInfo(String accountnumber) {
		if (clients.containsKey(accountnumber)) {
			Account account = clients.get(accountnumber);
			System.out.println("----------------------------");
			System.out.println("Firstname " + account.getFirstname());
			System.out.println("Lastname " + account.getLastname());
			System.out.println("Account " + account.getAccountNumber());
			System.out.println("Account " + account.getBalance());

		} else {
			System.out.println("ERROR : Cannot find account");
		}

	}

	public static void transfer(String fromAccount, String toAccount, int sum) { // �lekanne ise
		if (!clients.containsKey(fromAccount)) { // kontrollime kas konto on �ldse olemas
			System.out.println(" Errror fromAccount ");
			return;
		}
		if (!clients.containsKey(toAccount)) {
			System.out.println(" Errorr toAccount");
			return;
		}

		Account maksjaAccount = clients.get(fromAccount);
		Account saajaAccount = clients.get(toAccount);

		if (maksjaAccount.getBalance() < sum) {
			System.out.println(" Error raha pole piisavalt");

		}
		maksjaAccount.setBalance(maksjaAccount.getBalance() - sum);
		saajaAccount.setBalance(saajaAccount.getBalance() + sum);
		System.out.println("�lekanne edukalt teostatud");

	}

	public static void displayClientInfo(String Firstname, String Lastname) {
		for (Account account : clients.values()) {
			if (account.getFirstname().equalsIgnoreCase(Firstname)
					&& account.getLastname().equalsIgnoreCase(Lastname)) {

				printAccountDetails(account);
				return;
			}
		}

		System.out.println("ERROR : Cannot find account");

	}

	private static void printAccountDetails(Account account) {
		System.out.println("----------------------------");
		System.out.println("Firstname " + account.getFirstname());
		System.out.println("Lastname " + account.getLastname());
		System.out.println("Account " + account.getAccountNumber());
		System.out.println("Account " + account.getBalance());
		return;
	}

	
}