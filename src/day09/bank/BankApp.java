package day09.bank;

import java.io.IOException;
import java.util.Scanner;

public class BankApp {

	public static void main(String[] args) throws IOException {
		System.out.println("Panagas�steem alustab t��d");
		Account.importClients(args[0]);

		System.out.println("The bank has the following customers:");
		System.out.println(Account.getClients());

		Scanner inputScanner = new Scanner(System.in);
		while (true) {
			System.out.println("Palun sisesta k�sklus");
			// Kuula sisendit. Sisendi kuulamiseks vaja scanner objekti.
			String input = inputScanner.nextLine();

			// Teosta operatsioon.Mida me sisendiga peale hakkame? Kuidas String algab..
			if (input.startsWith("TRANSFER")) {
				System.out.println("Starting money transfer...");
				String[] inputParts = input.split(" ");
				Account.transfer(inputParts[1], inputParts[2], Integer.parseInt(inputParts[3]));

			} else if (input.startsWith("BALANCE")) {
				System.out.println("Displaying account balance...");
				String[] inputParts = input.split(" ");
				if (inputParts.length == 2) {
					System.out.println("Searching by account number");

					Account.displayClientInfo(inputParts[1]); // ei tagasta midagi (sisendiks on

				} else {
					System.out.println("Incorrect command");
					Account.displayClientInfo(inputParts[1], inputParts[2]);
				}

			}

		}
	}

}
