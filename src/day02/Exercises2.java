package day02;

public class Exercises2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Hello,World!");
		System.out.println("Hello,\"World\"!");
		System.out.println("Steven Hawking once said: \"Life would be tragic if it werent funny\".");
		
		String tallinnpopulation ="450 000" ;
		System.out.println("Tallinnas elab " + tallinnpopulation + " inimest");
		String booktitle = " Rehepapp" ;
		System.out.println("Raamatu \"" + booktitle + "\" autor on Andrus Kivirähk " ) ;
		
		String planet1 = "Merkuur" ;
		String planet2= "Venus" ;
		String planet3 = "Maa" ;
		String planet4 = "Marss" ; 
		String planet5= " Jupiter" ;
		String planet6= " Saturn";
		String planet7= "Uran";
		String planet8= "Neptuun" ;
		int planetCount= 8 ;
		System.out.println(String.format(" %s, %s, %s, %s, %s, %s, %s, %s on Päikesesüsteemi %s planeeti. ", planet1, planet2, planet3, planet4, planet5, planet6, planet7, planet8, planetCount)); 
		System.out.println(planet1 +"," planet2 + "," planet3 +"," planet4 +" ,"planet5 +" ," planet6 +" ," planet7 +"ja"+ "planet8 +" on Päikesesüsteemi "+ planetCount+ " planeeti" );
	}

}
