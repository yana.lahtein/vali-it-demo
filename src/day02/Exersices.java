package day02;

import java.math.BigDecimal;
import java.math.BigInteger;

public class Exersices {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Defineeri muutuja, mis hoiaks v��rtust 456.78.
		float f1 = 456.78F;
		double d1 = 456.78d;
		BigDecimal bd1 = new BigDecimal("456.78");

		String s1 = "test";
		boolean a1 = true;

		// Muutuja,mis viitab Numbritekogumile, variant 1
		int[] numArray1 = new int[4];
		numArray1[0] = 5;
		numArray1[1] = 91;
		numArray1[2] = 304;
		numArray1[3] = 405;
		// Muutuja,mis viitab Numbritekogumile, variant 2
		int[] numArray2 = { 5, 91, 304, 405 };

		float[] f2 = { 56.7f, 45.8f, 91.2f };
		String s2 = "a";
		char c1 = 'a';
		byte b2 = (byte) 'a';
		System.out.println((char) c1);

		// erineva iseloomuga muutujate kogum
		String[] textArray = { "see on eismene v��rtus", "67", "58.92" };

		// Suur number
		BigInteger bi1 = new BigInteger("7676868683452352345324534534523453245234523452345234523452345");

	}

}
