package day09_uuspaev;

import day08.Car;
import day08.audiA6;

public class App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Car audiA601 = new audiA6();
		audiA601.setColor("blue");
		audiA601.setmaxSpeed(55);

		Car audiA602 = new audiA6();
		audiA602.setColor("blue");
		audiA602.setmaxSpeed(20);

		System.out.println("Audi A6 1 hascode:" + audiA601.hashCode());
		System.out.println("Audi A6 1 hascode:" + audiA602.hashCode());

		audiA601 = audiA602;
		System.out.println(audiA601 == audiA602);
		System.out.println(audiA601.equals(audiA602));

		System.out.println();

		Car audiA603 = new audiA6();
		audiA603.setColor("red");
		audiA603.setmaxSpeed(33);

		Car audiA604 = new audiA6();
		audiA604.setColor("yellow"
				+ "");
		audiA604.setmaxSpeed(44);

		System.out.println(audiA603.compareTo(audiA604));
	}

}
