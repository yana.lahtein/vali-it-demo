import java.util.Random;

public class day04_tsuklid {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Ulesanne 11: Kirjuta while-tsükkel, mis prindiks standardväljundisse numbrid
		// 1 … 100 - iga number eraldi real.

		// int u= 1 ;
		// while (u <= 100 ) {
		// System.out.println(u++);

		// Ulesanne 12: Kirjuta for-tsükkel, mis prindiks standardväljundisse numbrid 1
		// … 100 - iga number eraldi real.

		// for (int x=1 ; x <= 100 ; x++) {
		// System.out.println(x);
		//
		// }
		//

		// Ülesanne 13: ● Kirjuta foreach-tsükkel, mis prindiks standardväljundisse
		// numbrid 1 … 10 - iga number eraldi real.

		// // Ulesanne 14
		// for (int x = 1; x <= 100; x++) {
		// if (x % 3 == 0) {
		// System.out.println(x);
		// }

		// Ülesanne 15: ● Defineeri massiiv väärtustega “Sun”, “Metsatöll”, “Queen”,
		// “Meallica”. ●
		// Prindi selle massiivi väärtused for-tsükli abiga standardväljundisse,
		// eraldades nad komaga.
		// Viimase elemendi järele koma panna ei tohi. Tulemus peaks olema: “Sun,
		// Metsatöll, Queen, Metallica”.

		// String[] arr2 = { "Sun ", " Metsatöll", " Queen", " Metallica"};
		// for( int i = 0 ; i < arr2.length ; i++) {
		// if ( i < arr2.length - 1) {
		// System.out.println(arr2[i]);
		// }

		// Ülesanne 16: ● Tee sama, mis eelmises ülesandes, ainult et prindi bändid
		// välja tagant poolt ettepoole.
		// ÜLesanne 17:

		// for (String numString : args) {
		// int num = Integer.parseInt(numString);
		// switch (num) {
		// case 0:
		// System.out.println("null");
		// break;
		// case 1:
		// System.out.println("üks");
		// break;
		// case 2:
		// System.out.println("kaks");
		// break;
		// case 3:
		// System.out.println("kolm");
		// break;
		// case 4:
		// System.out.println("neli");
		// break;
		// case 5:
		// System.out.println("viis");
		// break;
		// case 6:
		// System.out.println("kuus");
		// break;
		// case 7:
		// System.out.println("seitse");
		// break;
		// case 8:
		// System.out.println("kaheksa");
		// break;
		// case 9:
		// System.out.println("üheksa");
		// break;
		// }
		//
		// }

		// UL 18 do while

		do {
			System.out.println("tere");
		} while (false);

		double randomNumber;
		do {
			System.out.println("hello");
			randomNumber = (new Random()).nextDouble();
		} while (randomNumber < 0.5d);

		
		double randomNumber;
		do {
			System.out.println("hei");
			randomNumber = Match.Random();
		} while (randomNumber < 0.5d);

	}

}