
public class day04 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// // Ts�kli n�ide
		int[] arr1 = { 100, 200, 222, 333, 555 }; // massiivi n�ide
		//
		// for (int i= 0; i < arr1.length ; i++ ) { // suurendab 1 v�rra
		// System.out.println("tere "+ arr1[i] );
		// }
		//
		// for (int i = arr1.length -1 ; i>= 0 ;i--) { // v�hendab 1 v�rra
		// System.out.println("tere "+ arr1[2] );
		// }

		// for

		// int u= 0 ;
		// while (u < arr1.length) {
		// System.out.println( "tere " + arr1 [u++]);

		// }

		// int j= arr1.length -1 ;
		// while ( j >= 0) {
		// System.out.println(" tere " + arr1[j++] );
		// }

		// FOREACH
		for (int t : arr1) {
			System.out.println("Tere " + t);
			break;
		}

		String[] strArr = { "yks", "kaks", "kolm" };
		for (int i = 0; i < strArr.length; i++) {
			System.out.println(strArr[i]);

		}

		// DO-WHILE

		int k = 0;
		do {
			System.out.println(" tere " + arr1[k++]);
		} while (k < arr1.length);

		// BREAK
		for (int i = 2; i < 10000; i++) {
			if (i % 650 == 0) {
				System.out.println(i);
				break; // l�petab ts�kliga
			}
		}

		// continue
		for (int i = 1; i < 100; i++) {
			if (i < 6500) {
				System.out.println(i);
				continue; // j�tkab uue ts�kliga
			}

		}
		// Funktsiooni v�lja kutsumine
		someFunctionWithTwoLoops();
	}

	// �he ts�kli sees on teine ts�kkel
	private static void someFunctionWithTwoLoops() {
		for (int i = 0; i < 10; i++) {
			for (int s = 456; s < 634; s++) {
				if (s == 500) {
					break;
				}
			}

		}
	}

}
