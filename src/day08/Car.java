package day08;

import java.util.Objects;

public abstract class Car implements Movable , Comparable <Car> {

	private String color;
	private int maxSpeed;
	

	@Override
	public void setColor(String color) {
		this.color = color;

	}

	@Override
	public String getColor() {

		return this.color;
	}

	@Override
	public int getmaxSpeed() {

		return this.maxSpeed;
	}

	@Override
	public void setmaxSpeed(int kiirus) {
		this.maxSpeed = kiirus;
	}

	@Override
	public void drive() {
		// TODO Auto-generated method stub

	}

	// @Override
	// public boolean equals(Object obj) {
	// // TODO Auto-generated method stub
	// return super.equals(obj);
	// }
	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		Car teineauto = (Car) obj;
		boolean colorSame = this.getColor().equals(teineauto.getColor());
		boolean speedSame = this.getmaxSpeed() == (teineauto.getmaxSpeed());

		return colorSame && speedSame;

	}

	@Override
	public int hashCode() {

		int muutuja = 5;
		int varvmuutuja = Objects.hashCode(this.getColor());
		int kiirusmuutuja = this.getmaxSpeed();

		return 123 * varvmuutuja * kiirusmuutuja + muutuja;

	}
	
	public int compareTo(Car y) {
		
		return this.getmaxSpeed()+y.getmaxSpeed();
	}

}
