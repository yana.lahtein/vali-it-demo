package day08;

import static org.junit.Assert.*;

import java.math.BigInteger;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class AppTest {

	@BeforeClass // Meetod, mis k�ivitatakse k�ige ennem.
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception { // Viimane meetod, mida igal juhul k�ivitatakse.
	}

	@Before //
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		fail("Not yet implemented"); // fail on selline meetod, mis kukutab testi l�bi. T�estamiseks,et unit test
										// t��tab
	}

	@Test
	public void testAddNumbers() {
		int result = App.addNumbers(2, 2);
		assertTrue(result == 4); // abimeetod assert, testib kas tulemus on "EI"
		assertEquals(4, result);

	}

	@Test
	public void testgetBirthDayOfMonth() {
		boolean result = koer.Person.getBirthDayOfMonth(new BigInteger("48807172748"));
		assertTrue(result);

		result = koer.Person.getBirthDayOfMonth(null);
		assertFalse(result);

	}

	@Test
	public void testIsPersonalCodeCorrect() {
		boolean result = koer.Person.IsPersonalCodeCorrect("48807172748");
		assertTrue(result);

		result = koer.Person.IsPersonalCodeCorrect("48807172748");
		assertFalse(result);
		
		result = koer.Person.IsPersonalCodeCorrect("48807172748");
		assertFalse(result);
		
		result = koer.Person.IsPersonalCodeCorrect("8807172748");
		assertFalse(result);

	}
}
