package day08;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DateBasedEncryptor extends Cryptor {

	public DateBasedEncryptor(String filePath) throws IOException {
		super(filePath);
	}

	@Override
	protected Map<String, String> generateDictionary(List<String> fileLines) {
		Map<String, String> abc = new HashMap<>();
		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH);
		int day = cal.get(Calendar.DATE);
		int abinumber = year + month + day;

		for (String ukstaht : fileLines) {
			char c = ukstaht.charAt(0);
			int charValue = (int) c;
			String encodedValue = String.valueOf(charValue + abinumber);

			abc.put(ukstaht, encodedValue);
			// String[] lineParts = ukstaht.lineParts + char('loetelu'+ '2018 + 4 + 17');
			// dictionary.put(lineParts[0]);
		}
		return abc;

	}

	@Override // meetodi �mber kirjutamine p�hiklassist
	public String translate(String text) {
		String krupteeritudtext = "";
		char[] d = text.toUpperCase().toCharArray();
		for (char r : d) { // r- �ksik element, d on chari massiivi sees loetelu
			String x = String.valueOf(r);
			krupteeritudtext = krupteeritudtext + this.dictionary.get(x) + ";";
			// this.dictionary.get.String.valueof(d) ;
			// System.out.println("Krupteeritud kiri : " + String.valueOf(d) );
		}
		return krupteeritudtext;
	}

}
