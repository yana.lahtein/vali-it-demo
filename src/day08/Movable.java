package day08;

public interface Movable {

	void setColor(String color); // void- meetod, setColor- tagastustüüp, (string, color)-sisendparameeter
	String getColor ();
	
	void setmaxSpeed (int maxSpeed);
	int getmaxSpeed ();
	
	void drive ();
	
	
}
